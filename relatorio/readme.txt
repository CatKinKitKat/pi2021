Falta:
cap 1.2: remover o texto para colocar algures
cap 1.4: n sei se é necesssário colocar tb a referencia do git, do trello consegui, quando a professora diz que necessita de referência respetivamente ao GitHub, usei um \href que envia para lá, fiz o mesmo no ponto 3.1.2 e outros
cap 2: colocar + referencias bibliográficas
cap 2.7.3: alterei pré-modelação para pré-processamento uma vez que n era usado no texto, releiam para certificar que ficou a fazer sentido
cap 2: ler final do cap (nota da professora)
cap 3.2.3 / 3.3.3: add imagens .csv

=================================
Juntar caps 4, 5 e 6 em apenas 1.
=================================

cap 4.1: verificar se a explicação para o NFKD é suficiente.
cap 5.1: verificar se a apêndice IX é a correta a ser mostrada.
cap 5.2: mostrar um resultado.
cap 6.1: indicar qual a machine learning que optámos
cap 6.1: identificar o modelo mais adequado aos objetivos do projeto
cap 6.2 e 6.2.2: o que a professora referiu na correção
cap 6.3: indicar anexos
cap 8: ao que parece a professora não tinha a noção que fizemos uma base de dados, parece que não vê motivo para a coisa, ou retiramos as cenas do relatório referentes a isso, ou explicamos que foi feita para fazer os gráficos no powerBI como referi na algures na introdução.
conclusão: acrescentar as UCs que foram úteis para a realização do trabalho, sugestão: LP-José Jasnau; ES-Isabel Brito; BD1/BD2-Elsa e Isabel Brito; SI-Isabel Brito

Reler o relatório, pois podem existir possíveis erros devido a adaptação de palavras para outras palavras, como sentiments/sentiment -> sentimentos/sentimento