Este conjunto de ficheiros pretende facilitar a escrita de dissertações e relatórios de estágio ou projecto final de curso do Instituto Politécnico de Beja.

Os documentos gerados seguem (ou pelo menos estão muito próximos) do recomendado pelo respectivo documento normativo do IPBeja intitulado " Normas Obrigatórias para a Elaboração de Documentos Institucionais e Trabalhos Académicos".

O ficheiro principal tem o nome "01-main-document.tex". É este que permite gerar o relatório. A informação "base" (titulo, autor, etc.) deve ser colocada no ficheiro "02-info-to-fill.tex" nos locais lá assinalados. A bibliografia deve ser colocada no ficheiro "03-bibliography.bib". O conteúdo do relatório deve ser colocado em ficheiros nas seguintes directorias:
"parte-inicial" resumo, agradecimentos, etc.
"capitulos" texto dos capítulos
"figuras" para todas as imagens
"listagens" para listagens de código
"apendices" se existentes
"anexos" se existentes

Note que todos os ficheiros nesta distribuição são oferecidos sem qualquer garantia, expressa ou implícita. O utilizador assume todos os riscos na sua utilização. Nem o autor nem o Instituto Politécnico de Beja podem ser responsabilizados por quaisquer danos resultantes da utilização de qualquer destes ficheiros.

Esta distribuição e os seus ficheiros podem ser livremente utilizados e modificados desde que se mantenham todas as referências à sua autoria e origem.

O Henrique Água-Doce, o Nuno Mourinho e o Raul Carvalho contribuíram com sugestões concretas para melhorar este template. Deixo aqui o meu agradecimento a ambos.

Agradeço que quaisquer correcções ou sugestões de melhoria sejam enviadas para joao.barros@ipbeja.pt.

João Paulo Barros
Beja, 18 de abril de 2013
Beja, 10 de outubro de 2013
Beja, 3 de julho de 2014
Beja, 3 de novembro de 2020
Beja, 20 de novembro de 2020
Beja, 4 de janeiro de 2021
Beja, 1 de outubro de 2021