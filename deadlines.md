# Deadlines

## Fase 1

### Web-scraping

Data(1 Dezembro)

Quatro sub-fases:

+ TripAdvisor: Amaro
+ Booking: Pedro
+ Zomato: Vítor
+ Google Maps*: Todos
+ Relatório de Fase: Todos

*- Se conseguirmos.

## Fase 2

### Text-Analysis/Machine Learning

Data(18 Dezembro) -> com possibilidade de revisão durante as férias

Quatro sub-fases iniciais:

+ TripAdvisor: Amaro
+ Booking: Pedro
+ Zomato: Vítor
+ Google Maps*: Todos
+ Relatório de Fase: Todos

*- Se conseguirmos.

Sub-fases seguinte:

+ Cross-references entre posts e keywords: Pedro e Vítor
+ Cross-references entre plataformas: Amaro
+ Relatório de Fase: Todos

## Fase 3

### Apresentação gráfica e possíveis revisões às fases anteriores

Data(30 Janeiro)

Single sub-fases:

+ Criar os gráficos: Todos*
+ Revisões: Todos
+ Relatório de Fase: Todos

*- Gráficos específicos de cada plataforma, feitos pelo individuo responsável.

## Fase 4

### Relatório LaTeX (template do JPB)

Data(10 Fevereiro)

Sub-fases:

+ Relatório Final: Todos
+ Possíveis correções de conselhos da docente: Todos
